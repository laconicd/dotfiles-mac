# functions
def GPU [] {
  git add .
  git commit -m (date now | format date '%Y.%m.%d')
  git push origin main
}

def S [] {
  let SVR = (rg ^Host ~/.ssh
    | detect columns -n
    | get column1
    | filter {|el| $el != '*'}
    | to text
    | fzf)
  if not ($SVR | is-empty) {
    ssh $SVR
  }
}

# Aliases
alias l = ls
alias ll = ls -a
alias lll = ls -al
alias x = hx

# Starship
use ~/.cache/starship/init.nu
