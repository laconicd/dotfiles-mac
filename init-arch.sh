#!/usr/bin/env bash

# install packages
sudo pacman -Syy git base-devel \
  fzf nushell starship helix ripgrep fd gitui \
  ttf-victor-mono-nerd noto-fonts-cjk 

# copy configs
cp -r .config/fontconfig ~/.config
cp -r .config/alacritty ~/.config
cp -r .config/helix ~/.config
cp -r .config/nushell ~/.config
cp -r .config/starship.toml ~/.config

# ansible
sudo pacman -S python-pipx sshpass
pipx install --include-deps ansible jmespath pynetbox pywinrm netaddr

# rust
sudo pacman -S rustup rust-analyzer
rustup default stable

# starship
mkdir -p ~/.cache/starship
starship init nu | save -f ~/.cache/starship/init.nu
