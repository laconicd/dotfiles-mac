#!/usr/bin/env zsh

# install honebrew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# install packages
/opt/homebrew/bin/brew tap homebrew/cask-fonts
/opt/homebrew/bin/brew install rectangle maccy iina iterm2 alacritty google-chrome \
zellij nushell starship helix ripgrep fd gitui fzf \
font-victor-mono-nerd-font \
rustup rust-analyzer

# copy configs
cp -r .config/helix ~/.config/
cp -r .config/nushell ~/Library/Application\ Support
cp -r .config/starship.toml ~/.config

# starship
mkdir ~/.cache/starship
starship init nu > ~/.cache/starship/init.nu
